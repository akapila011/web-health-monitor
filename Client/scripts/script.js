
function validateIPField() {
    var ip = document.getElementById('input-ip').value
    if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip)) {
    return ip;
  }
  alert("Please enter a valid IPv4 Address");
  return "-1";    // error code
}   // end validateIPField()

function TryParseInt(str, defaultValue) {
    // Get's int from string, otherwise returns the second parameter
     var retValue = defaultValue;
     if(str !== null) {
         if(str.length > 0) {
             if (!isNaN(str)) {
                 retValue = parseInt(str);
             }
         }
     }
     return retValue;
}   // end TryParseInt()

function setTemp(temp) {
    $("#temperature-value").removeClass();
    $("#temperature-value").text(temp);
    if (temp < 20) {
        $("#temperature-value").addClass("cold");
    }
    else if (temp < 25) {
        $("#temperature-value").addClass("okay");
    }
    else if (temp < 38) {
        $("#temperature-value").addClass("warm");
    }
    else if (temp > 38) {
        $("#temperature-value").addClass("hot");
    }
}  // end setTemp()

function setMotion(m) {
    $("#motion-value").removeClass();
    $("#motion-value").addClass("steady");
    var moving = "Steady";
    if (m == 1) {
        moving = "Moving";
        $("#motion-value").removeClass();
        $("#motion-value").addClass("moving");
    }
    $("#motion-value").text(moving);
}  // end setMotion()

function setAlcohol(volt) {
    $("#alcohol-value").removeClass();
    var bac = volt / 1000
    if (bac > 0.08 && bac < 0.1) {
        $("#alcohol-value").addClass("buzzed");
    }
    else if (bac >= 0.1 ) {
        $("#alcohol-value").addClass("drunk");
    }
    else {
        $("#alcohol-value").removeClass();
    }
    $("#alcohol-value").text(bac);
}  // end setAlcohol()

function getTimeInterval() {
    var e = document.getElementById("interval-time");
    var strTime = e.options[e.selectedIndex].text;
    var intTime = TryParseInt(strTime, 2);
    return intTime;
}   // end getTimeInterval()

function disableForm() {
    $('#input-ip').attr('disabled', true);
    $('#refresh-btn').attr('disabled', true);
    $('#interval-time').attr('disabled', true); 
}   // end disableForm()

function getMetrics(dataUrl) {
  $.ajax({
    url :  dataUrl,
    type: "GET",
    dataType: "json",
    success: function(response)  {
        //var data = JSON.stringify(response);
        //console.log(response);
        $("#error-msg").text("");
        //var time = new Date();
        //$("#time-label").text("Time: " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds())
        $("#time-label").text(new Date().toLocaleTimeString());

        setTemp(response["temperature"]);
        $("#humidity-value").text(response["humidity"]);
        setMotion(response["motion"]);
        setAlcohol(response["bac"]);
    },   // end success()
    error: function(jqXHR, errorThrown) {
        //alert("Could not retrieve metrics from the specified device.");
        $("#error-msg").text(errorThrown.Message);
        //alert(errorThrown);
        //var time = new Date();
        //$("#time-label").val("Time: " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds())
        $("#time-label").text(new Date().toLocaleTimeString());

        setTemp(0);
        $("#humidity-value").text(0);
        setMotion("Steady");
        $("#alcohol-value").text(0.00);
    }   // end fail()
  })   // end ajax{}
}    // end getMetrics()

function getData() {
    var ip = validateIPField();
    var dataUrl = "http://" + ip;
    getMetrics(dataUrl)
}   // end getData()


$(document).ready(function() {
  $("#refresh-btn").on("click", function() {
    var intTime = getTimeInterval() * 1000;
    
    var ip = validateIPField();
    if (ip != "-1") {
        disableForm();
        setInterval(getData, intTime);
    }
  }); // END #refresh-btn callback
});  // END doc ready