# Arduino Web Health Monitor

The arduino web health monitor is a arduino-based system that measures various parameters such as temperature, humidity, motion and alcohol levels.
The system consists of the arduino server system that retrieves sensor readings and is connected to using an Ethernet shield to serve this data as a json response to clients. The other component is the client webpage which provides an easy to use interface to view the readings fetched from the arduino.

![Web Health Monitor](Resources/health-interface.PNG "Web Health Monitor")

## How to Use
1. The sensors must be setup and connected to the arduino where output values from the sensor modules should be connected to the arduino pins as specified in the source code.
2. The arduino can be powered on, code uploaded and run. Once the program is running the network configurations will be displayed and readings will be shown once a request comes in. You can also visit the IP address of the arduino to retrieve a json format of the readings.

**N.B** Current settings require a DHCP server to assign the arduino an IP. The serial monitor will display this IP once the networking has been setup. When working with a headless setup you can assign static network configurations.

3. Now you can run a simple web server to serve the index.html file located withing the 'Client' folder. I used a simply python webserver that comes bundled along with any python installation. To run the web server using python 3, navigate to the folder where the index.html file is located and run the following 
```bash
$> python3 -m http.server 80
```
You can now navigate to [localhost](http://127.0.0.1) for a more user-friendly interface.
4. Once you have navigated to the user interface, you simply have to input the IP of the arduino that is running, select a refresh interval and hit refresh for the values to constantly be updated.

### Hardware
Hardware used in this project includes:
>- [A PIR motion sensor](https://www.amazon.com/Blika-HC-SR501-Pyroelectric-Infrared-Microcontrollers/dp/B0773G5H7N/ref=sr_1_1_sspa?ie=UTF8&qid=1512230701&sr=8-1-spons&keywords=pir+motion+sensor&psc=1)
>- [A DHT11 temperature/humidity sensor](https://www.amazon.com/Qunqi-DHT11-Temperature-Humidity-Raspberry/dp/B014P4WVIK/ref=sr_1_1_sspa?ie=UTF8&qid=1512230680&sr=8-1-spons&keywords=dht11&psc=1) 
>- [An MQ3 alcohol sensor](https://www.amazon.com/WINGONEER-10-1000ppm-Alcohol-Sensor-Detector/dp/B06XHJDG1K/ref=sr_1_1?ie=UTF8&qid=1512230742&sr=8-1&keywords=mq3+alcohol+sensor)
>- [An ENC28J60 Ethernet module](https://www.amazon.com/SunFounder-ENC28J60-Ethernet-Network-Arduino/dp/B00GAXAQOQ/ref=sr_1_4?ie=UTF8&qid=1512230726&sr=8-4&keywords=ENC28J60+ethernet)
>- [Arduino Mega 2560](https://www.amazon.com/Elegoo-Board-ATmega2560-ATMEGA16U2-Arduino/dp/B01H4ZLZLQ/ref=sr_1_1_sspa?s=industrial&ie=UTF8&qid=1512230806&sr=1-1-spons&keywords=arduino+mega+2560&psc=1)

### Libraries Used
To use the DHT11 and ENC28J60 module the following libraries were used:
>- [DHT11 Library](http://www.circuitbasics.com/wp-content/uploads/2015/10/DHTLib.zip)
>- [ENC28J60 Library](https://github.com/jcw/ethercard/blob/master/README.md)


### Warning
The MQ3 alcohol sensor module requires a few minutes to heat up. In general, the higher the MQ3 value output the more alcohol in the patient's system. This is by no means an alternative to a breathlyzer as the MQ3 requires a lot more calibration before it can provide fully reliable blood alcohol level values.