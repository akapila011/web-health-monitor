#include <dht.h>
#include <EtherCard.h>

#define STATIC 0  // set to 1 to disable DHCP (adjust myip/gwip values below)

#if STATIC
// ethernet interface ip address
static byte myip[] = { 192,168,1,202 };
// gateway ip address
static byte gwip[] = { 192,168,0,1 };
#endif
// ethernet mac address - must be unique on your network
static byte mymac[] = { 0x74,0x69,0x69,0x2D,0x30,0x31 };
byte Ethernet::buffer[500]; // tcp/ip send and receive buffer
BufferFiller bfill;

// Sensor pins
int pir_pin = 5;
int alcohol_pin = 0; // analog
#define DHT11_PIN 7

// Sensor values
double temperature = 0.0;  // Celcius
int abs_temperature = 0;
double humidity = 0.0;
int abs_humidity = 0;
bool motion = false;
String moving = "Not Moving";
int alcohol_voltage = 0;
float bac = 0;

// Support variables
int pir_state = LOW;
int pir_val = 0;
dht DHT;

void findTemperatureHumidity() {
  // s = digital 7, middle = 5v, - = gnd
  int chk = DHT.read11(DHT11_PIN);
  temperature = DHT.temperature;
  abs_temperature = abs((int)temperature);
  humidity = DHT.humidity;
  abs_humidity = abs((int)humidity);
}

void detectMotion() {
  // middle = digital 5
  pir_val = digitalRead(pir_pin);
  if (pir_val == HIGH) {
    motion = true;
    moving = "Moving";
    pir_state = HIGH;
  }
  else {
    if (pir_state == HIGH) {
      motion = false;
      moving = "Not Moving";
      pir_state = LOW;
    }
  } // end else
}  // end detectMotion()

void detectAlcohol() {
  // uses A0
  alcohol_voltage = analogRead(alcohol_pin);
  bac = alcohol_voltage / 1000;
}  // end detectAlcohol()

static word respond_json() {
  /*
  vcc = 3.3v
  gnd = gnd
  sck = digital 52
  so = digital 50
  si = digital 51
  cs = digital 53
  */
  findTemperatureHumidity();
  detectMotion();
  detectAlcohol();
  bfill = ether.tcpOffset();
  bfill.emit_p(PSTR(
    "HTTP/1.1 200 OK\r\n"
    "Content-Type: application/json;charset=utf-8\r\n"
    "Access-Control-Allow-Origin: *\r\n"
    "Server: Arduino\r\n"
    "Connection: close\r\n"
    "\r\n"
    "{\"temperature\": $D, \"humidity\": $D, \"motion\": $D, \"bac\": $D}")
    , abs_temperature, abs_humidity, motion, alcohol_voltage);
    return bfill.position();
}  // end respond_json()

void printValues() {
  Serial.print("Temperature: "); Serial.print(temperature); Serial.println(" degrees Celcius");
  Serial.print("Humidity: "); Serial.print(humidity); Serial.println(" %");
  Serial.print("Patient Moving: "); Serial.println(moving);
  Serial.print("Blood Alcohol Content: "); Serial.println(bac);
}  // end printValues()

void setup() {
  // put your setup code here, to run once:
  pinMode(pir_pin, INPUT);
  Serial.begin(9600);
  Serial.println("[Web Health Monitor]");
  if (ether.begin(sizeof Ethernet::buffer, mymac, 53) == 0) 
    Serial.println( "Failed to access Ethernet controller");
  #if STATIC
    ether.staticSetup(myip, gwip);
  #else
    if (!ether.dhcpSetup())
      Serial.println("DHCP failed");
  #endif
  
  ether.printIp("IP:  ", ether.myip);
  ether.printIp("GW:  ", ether.gwip);  
  ether.printIp("DNS: ", ether.dnsip);
}

void loop() {
  // put your main code here, to run repeatedly:
  // wait for an incoming TCP packet, but ignore its contents
  word len = ether.packetReceive();
  word pos = ether.packetLoop(len);
  if (pos) {
    ether.httpServerReply(respond_json());
    printValues();
  }
}


